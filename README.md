# Pagina web para el procesamiento de imagenes

## Imagen de prueba
![alt text](static/images/exp_5.jpg)

### Iniciando 

![alt text](resultados/iniciando.png)

## Imagenes de resultado

### Pagina principal

![alt text](resultados/principal.png)

### Imagen seleccionada

![alt text](resultados/seleccionar.png)

### Metodos

![alt text](resultados/metodos.png)

### Aplicando operador logaritmo

![alt text](resultados/logaritmo.png)

### imagen resultado

![alt text](resultados/antes_contrast.png)

### Aplicando contrast stretching

![alt text](resultados/contrast.png)

### Aplicando equalizacion del histograma

![alt text](resultados/equalizacion.png)

### Aplicando operador exponecial

![alt text](resultados/exponencial.png)

### Aplicando operador raiz cuadrada

![alt text](resultados/raizC1.png)

### Aplicando operador raiz cuadrada

![alt text](resultados/raizC2.png)
