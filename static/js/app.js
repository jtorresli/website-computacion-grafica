$(document).ready(function() {

    $("#file").on('change', function (event){
        event.preventDefault();
        var propiedad = document.getElementById("file").files[0];
        
        var nombre_imagen = propiedad.name;
        var extension_image = nombre_imagen.split('.').pop().toLowerCase();
        if(jQuery.inArray(extension_image, ['gif', 'png', 'jpg', 'jpeg']) == -1)
        {
            alert("No es un archivo de imagen");
        }
        var image_size = propiedad.size;
        if(image_size > 2000000) {
            alert("La imagen es muy grande");
        }
        else {
            var form_data = new FormData();
            form_data.append("file", propiedad);

            $.ajax({
                data : form_data,
                type : 'POST', 
                url: '/mostrar',
                contentType: false,
                cache: false,
                processData: false
            })
            .done(function(data) {
                if(data.error) {
                    console.log("Error");
                }
                else {
                    console.log(data.name)
                    $("#imagen").prop("src", '/static/images/' + data.name);
                }
            });
        }        
    });

    $('form').on('submit', function(event) {
        event.preventDefault();     
        $.ajax({ 
            url: '/calcular',
            data: { porcentaje: $('#porcentaje').val(),
                    valor_a: $('#valor-a').val(),
                    valor_b: $('#valor-b').val(),
                    valor_c: $('#valor-c').val(),
                    valor_r: $('#valor-r').val(),
                    operador: $('#operador').val()
                },
            type: 'POST'
        }).done(function(data) {
            var source = '/static/images/' +data.name,
            timestamp = (new Date()).getTime(),
            newUrl = source + '?_=' + timestamp;
            document.getElementById("imagen").src = newUrl;
            
        }).fail(function() {
            console.log('Failed');
        });        
    });     

});